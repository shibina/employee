package Employee.emp.dao;

import java.util.Map;

import Employee.emp.entity.EmployeeDetails;
import Employee.emp.entity.LoginDetails;



public interface UserDao {

	public Map<String,Object> insertLoginDetails(LoginDetails login,EmployeeDetails emp);
	public LoginDetails getLoginInfo(String email);
	public Map<String,Object> saveEmployeeDetails(EmployeeDetails emp);
	public Map<String,Object> getuserDetails(long id);
}
