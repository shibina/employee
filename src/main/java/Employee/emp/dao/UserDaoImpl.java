package Employee.emp.dao;

import java.util.HashMap;

import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import Employee.emp.entity.EmployeeDetails;
import Employee.emp.entity.LoginDetails;

@EnableTransactionManagement
@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
@Repository("userDao")
public class UserDaoImpl implements UserDao{
	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private HibernateTemplate hibernateTemplate;
	
	private Session openDBsession() {
		Session session;
		session = sessionFactory.getCurrentSession();
		return session;
	}
	@Override
	public Map<String, Object> insertLoginDetails(LoginDetails login, EmployeeDetails emp) {
		Map<String, Object> map = null;
		try {
			Session session = openDBsession();
			
				Criteria criteria = session.createCriteria(LoginDetails.class);
				criteria.add(Restrictions.eq("userName", login.getUserName()));
				Criteria cr = criteria.createCriteria("empDetails");
				cr.add(Restrictions.eq("mailId", emp.getMailId()));
				List<LoginDetails> loginList = criteria.list();
				
				if (loginList.isEmpty() || loginList.size() == 0) {
					session.save(emp);
					session.flush();
					login.setEmployeeId(emp.getEmployeeId());
					session.save(login);
					session.flush();

					map = new HashMap<String, Object>();
					map.put("responseCode", 0);
					map.put("ResponseMessage","SaveSuccess");
					map.put("data", emp);
				} else {
					map = new HashMap<String, Object>();
					map.put("responseCode", -2);
					map.put("ResponseMessage","user already exist");

				}
			

		} catch (Exception e) {
			e.printStackTrace();
			map = new HashMap<String, Object>();
			map.put("responseCode", -2);
			map.put("ResponseMessage","error");

		}

		return map;
	}
	@Override
	public LoginDetails getLoginInfo(String email) {
		
		Map<String, Object> map = null;
		Session session = openDBsession();
		Criteria criteria = session.createCriteria(LoginDetails.class);
		Criteria cr = criteria.createCriteria("empDetails");
		cr.add(Restrictions.eq("mailId", email));
		LoginDetails user = (LoginDetails) criteria.list().get(0);
		
		return user;
	}
	@Override
	public Map<String, Object> saveEmployeeDetails(EmployeeDetails emp) {
		// TODO Auto-generated method stub
		Map<String, Object> map = null;
		try {
			Session session = openDBsession();
			Criteria criteria = session.createCriteria(EmployeeDetails.class);
			criteria.add(Restrictions.eq("mailId", emp.getMailId()));
			List<EmployeeDetails> loginList = criteria.list();
			if (loginList.isEmpty() || loginList.size() == 0) {
			session.save(emp);
			LoginDetails login = new LoginDetails();
			login.setUserName(emp.getMailId());
			login.setPassword(emp.getPassword());
			login.setEmployeeId(emp.getEmployeeId());
			session.save(login);
			session.flush();
			map = new HashMap<String, Object>();
			map.put("responseCode", 0);
			map.put("ResponseMessage","SaveSuccess");
			map.put("data", emp);
			}else{
				map = new HashMap<String, Object>();
				map.put("responseCode", -2);
				map.put("ResponseMessage","user mail exist");
				
			}
		} catch (Exception e) {
			e.printStackTrace();
			map = new HashMap<String, Object>();
			map.put("responseCode", -2);
			map.put("ResponseMessage","error");

		}
		return map;
	}
	@Override
	public Map<String, Object> getuserDetails(long id) {
		// TODO Auto-generated method stub
		
		Map<String, Object> map = null;
		try {
			Session session = openDBsession();
				Criteria criteria = session.createCriteria(LoginDetails.class);
				criteria.add(Restrictions.eq("loginId",id ));
				LoginDetails user = (LoginDetails) criteria.list().get(0);
				map = new HashMap<String, Object>();
				map.put("responseCode", 0);
				map.put("ResponseMessage","Success");
				map.put("data", user);
		
	} catch (Exception e) {
		e.printStackTrace();
		map = new HashMap<String, Object>();
		map.put("responseCode", -2);
		map.put("ResponseMessage","error");

	}
		
	return map;
	}
}
