package Employee.emp.service;

import java.util.Map;

import Employee.emp.entity.EmployeeDetails;
import Employee.emp.model.Response;

public interface UserService {

	public Response insertLoginDetails();
	
	public Response	authenticate(String email, String pwd);
	
	public Response saveEmployeeDetails(EmployeeDetails employee);
	public Map<String, Object>  getuserDetails (long id);
}
