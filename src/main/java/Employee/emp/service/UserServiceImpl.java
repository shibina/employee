package Employee.emp.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Employee.emp.dao.UserDao;
import Employee.emp.entity.EmployeeDetails;
import Employee.emp.entity.LoginDetails;
import Employee.emp.model.Response;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserDao userDao;

	@Override
	public Response insertLoginDetails() {
		// TODO Auto-generated method stub
		Map<String, Object> map = null;
		Response res = null;
		EmployeeDetails emp = new EmployeeDetails();
		emp.setName("admin");
		emp.setMailId("admin@gmail.com");
		emp.setDesignation("Admin");
		emp.setRole("admin");
		LoginDetails login = new LoginDetails();
		login.setUserName("admin");
		login.setPassword("admin");
		map = userDao.insertLoginDetails(login, emp);
		res = new Response();
		res.responseCode = (Integer) map.get("responseCode");
		res.responseMessage = (String) map.get("responseMessage");
		res.objectData = map;
		return res;

	}

	@Override
	public Response authenticate(String email, String pwd) {
		Response res = new Response();
		LoginDetails usersignup = userDao.getLoginInfo(email);
		if (usersignup != null) {
			if (usersignup.getPassword().equals(pwd)) {
				res.objectData = usersignup;
				res.responseMessage = "Success";
				res.responseCode = 0;
			} else {
				res.responseCode = 1;
				res.responseMessage = "Invalid password";
				res.objectData = null;
			}

		} else {
			res.responseCode = 2;
			res.responseMessage = "No such user exist";
		}
		return res;

	}

	@Override
	public Response saveEmployeeDetails(EmployeeDetails employee) {
		// TODO Auto-generated method stub
		Response res = new Response();
		Map<String,Object> map = userDao.saveEmployeeDetails(employee);
		
		res = new Response();
		res.responseCode = (Integer) map.get("responseCode");
		res.responseMessage = (String) map.get("responseMessage");
		res.objectData = map;
		return res;
	}

	@Override
	public Map<String, Object> getuserDetails(long id) {
		// TODO Auto-generated method stub
		Response res = new Response();
		Map<String,Object> map = userDao.getuserDetails(id); 
		
		return map;
	}

}
