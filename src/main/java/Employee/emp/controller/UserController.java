package Employee.emp.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import Employee.emp.entity.EmployeeDetails;
import Employee.emp.entity.LoginDetails;
import Employee.emp.model.Response;
import Employee.emp.service.UserService;

@RestController
public class UserController {
	@Autowired
	UserService userService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView test(HttpServletResponse response) throws IOException {
		return new ModelAndView("login");
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	private ModelAndView getUserDetails(@ModelAttribute LoginDetails login) {

		System.out.print(login.toString());
		return new ModelAndView("home");
	}

	@RequestMapping(value = "/activation", method = RequestMethod.GET)
	public Object activateadmin(HttpServletRequest req) {

		Response response = userService.insertLoginDetails();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("response", response);
		return new ModelAndView("login", map);

	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public Object postlogin(HttpServletRequest req, @ModelAttribute("login") LoginDetails login) {

		Map<String, Object> map = new HashMap<String, Object>();

		String userName = req.getParameter("userName");
		String passWord = req.getParameter("password");
		Response res = userService.authenticate(userName, passWord);
		System.out.println(res.toString());
		if (res.responseCode == 0) {
			HttpSession session = req.getSession();
			session.setAttribute("sessionScope", res.objectData);
			map.put("error_message", res.responseMessage);
			map.put("user",  res.objectData);
			return new ModelAndView("home", map);

		} else {
			map.put("error_message", res.responseMessage);
			return new ModelAndView("login", map);

		}
		
	}
	@RequestMapping(value = "/addEmployee", method = RequestMethod.GET)
	public ModelAndView getaddEmployeePage(HttpServletResponse response) {
		return new ModelAndView("employee");
	}

	@RequestMapping(value = "/employees", method = RequestMethod.POST)
	public Object saveemployeeDetails(HttpServletRequest req ,@ModelAttribute("employee") EmployeeDetails employee ) {
		System.out.println("hi");
		Map<String, Object> map= new HashMap<String, Object>();
		Response res = userService.saveEmployeeDetails(employee);
		map.put("response", res);
		return new ModelAndView("employee" ,map);
	}
	
	@RequestMapping(value = "getreport", method = RequestMethod.GET)
	public ModelAndView getReports(@RequestParam("userId") long userId,HttpServletRequest req ) throws IOException {
		Map<String, Object> map= userService.getuserDetails(userId);
		
		return new ModelAndView("report", map);
	}
}
