/**
 * 
 */
package Employee.emp.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "EMPLOYEE_DETAILS", uniqueConstraints = { @UniqueConstraint(columnNames = "EMPLOYEE_ID") })
public class EmployeeDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "EMPLOYEE_ID", unique = true, nullable = false)
	private long employeeId;
	
	@Column(name = "NAME", unique = false, nullable = true)
	private String name;
	
	@Column(name = "AGE", unique = false, nullable = true)
	private int age;

	@Column(name = "ADDRESS", unique = false, nullable = true)
	private String address;
	
	@Column(name = "MAIL_ID", unique = false, nullable = true)
	private String mailId;
	
	@Column(name = "DESIGNATION", unique = false, nullable = true)
	private String designation;
	
	@Column(name = "DEPARTMENT_ID", unique = false, nullable = true)
	private long departmentId;
	
	@Column(name = "ROLE", unique = false, nullable = true)
	private String role;

	@Transient
	private int project;
	@Transient
	private String password;
	
	@OneToMany(mappedBy="employeeDetails",fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	private List<Project> projectList;
	
	@OneToMany(mappedBy="empDetails",fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	private List<LoginDetails> loginList;

	
	
	public long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public long getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(long departmentId) {
		this.departmentId = departmentId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	

	public List<LoginDetails> getLoginList() {
		return loginList;
	}

	public void setLoginList(List<LoginDetails> loginList) {
		this.loginList = loginList;
	}

	

	public List<Project> getProjectList() {
		return projectList;
	}

	public void setProjectList(List<Project> projectList) {
		this.projectList = projectList;
	}

	public int getProject() {
		return project;
	}

	public void setProject(int project) {
		this.project = project;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
}
