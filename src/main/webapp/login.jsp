<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
        <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
            <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
            <html>

            <head>
                <meta charset="utf-8">
                <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Login </title>
                <link rel="stylesheet" type="text/css" href="public/bootstrap/css/bootstrap.min.css" />
                <link rel="stylesheet" type="text/css" href="public/font-awesome/css/font-awesome.min.css" />
                <link rel="stylesheet" type="text/css" href="public/css/local.css" />
                <script type="text/javascript" src="public/js/jquery-1.10.2.min.js"></script>
                <script type="text/javascript" src="public/bootstrap/js/bootstrap.min.js"></script>
                <link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
                <link id="gridcss" rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/dark-bootstrap/all.min.css" />
                <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
                <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
                <script type="text/javascript" src="http://www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>
                <script type="text/javascript" src="http://www.prepbootstrap.com/Content/js/gridData.js"></script>
                 <link rel="stylesheet" type="text/css" href="public/admi.css" />
                 <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.3.min.js"></script>
                <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>
                <script src="public/validation.js"></script>
              <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
            </head>

            <body>

              <div id="wrapper">
                    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="login">MALABAR MARKET</a>
                        </div>
                        <div class="collapse navbar-collapse navbar-ex1-collapse">
                            <div class="collapse navbar-collapse navbar-ex1-collapse">
                                <!-- /.container -->
                                <div class="market-home">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-md-4 col-md-offset-4 ">
                                                <div class="home-welcome">
                                                    <form:form method="post" action="home" commandName="homeKey" name="home">
                                                </div>
                                                <div class="home-link">
                                                   
                                                    <ul class="nav navbar-nav navbar-right navbar-user">
                                                        <li class="dropdown messages-dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> Messages <span class="badge"></span> <b class="caret"></b></a>
                                                            <ul class="dropdown-menu">
                                                                <li class="dropdown-header">2 New Messages</li>
                                                                <li class="message-preview">
                                                                    <a href="#">
                                                                        <span class="avatar"><i class="fa fa-bell"></i></span>
                                                                        <span class="message">Security alert</span>
                                                                    </a>
                                                                </li>
                                                                <li class="divider"></li>
                                                                <li class="message-preview">
                                                                    <a href="#">
                                                                        <span class="avatar"><i class="fa fa-bell"></i></span>
                                                                        <span class="message">Security alert</span>
                                                                    </a>
                                                                </li>
                                                                <li class="divider"></li>
                                                                <li><a href="#">Go to Inbox <span class="badge">2</span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li class="dropdown user-dropdown">
                                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <b class="caret"></b></a>
                                                            <ul class="dropdown-menu">
                                                                <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                                                                <li><a href="#"><i class="fa fa-gear"></i> Settings</a></li>
                                                                <li class="divider"></li>
                                                                <li><a href="#"><i class="fa fa-power-off"></i> Log Out</a></li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                    </form:form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>

                
                    <div class="container-fluid">
                        <div class="row">
                            <div class="log-row">
                                <div class="col-md-4 col-md-offset-4 log-new">
                                    
                                        <form:form method="post" action="login" commandName="loginKey" name="login" id="login">
                                            <div class="form-group">
                                                <label for="txt">Username</label>
                                                <input type="text" class="form-control" name="userName" id="usererror"/>
                                            </div>
                                            <div class="form-group">
                                                <label for="pwd"> Password </label>
                                                <input type="password" class="form-control" name="password" />
                                                <div class="butt-submit">
                                                <p style="color:red">${f_error_message}</p>
                                                    <br/>
                                                    <button type="submit"  class="btn btn-info">Login</button>
                                                </div>
                                            </div>
                                        </form:form>                                
                                </div>
                            </div>
                        </div>
                    </div>
                       <!--        <p style="color:red">Error Message:${f_error_message}${login_error}${access_error}</p> -->
             
                
            </body>

            </html>
