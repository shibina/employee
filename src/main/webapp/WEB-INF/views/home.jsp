<%@page contentType="text/html" pageEncoding="UTF-8"%>
    <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Home</title>
</head>
<body>
<h1>HOME PAGE</h1> ${sessionScope} -------------
  <p style="color:red">${error_message}</p>=================
	<div>
		<ul>
		<c:if test="${sessionScope.role==admin}">
			<li><a href="addEmployee">
					ADD EMPLOYEE</a></li>
			<li><a href="#">
					ADD NEW DEPARTMENT</a></li>
					<li><a href="#">
					ADD NEW PROJECT</a></li>
		</c:if>		
			<li><a href="getreport?userId="${sessionScope.loginId}>
					GET REPORT</a></li>
			<li><a href="#">LOG OUT</a></li>
		</ul>
	</div>
</body>
</html>
