<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>employee</title>
</head>
<body>
	<div class="container">
		<h2>ADD EMP</h2>
		<br>
		<p style="color: red">${error_message}</p>
		<form:form method="post" action="employees" commandName="employee" id="addemployee" name="addemployee">
			<table>
				<tr>
					<td><label> Choose Department </label></td>
					<td><select>
							<option value="1">D1</option>
							<option value="2">D2</option>
							<option value="3">D3</option>
							<option value="4">D4</option>
					</select></td>
				</tr>
				<tr>
					<td><label>Employee Name:</label></td>
					<td><input type="text" name="name" id="name" /></td>
				</tr>
				<tr>
					<td><label>Age</label></td>
					<td><input type="text" name="age" id="age" /></td>
				</tr>
				<tr>
					<td><label>Address</label></td>
					<td><textarea name="address" id="address"></textarea></td>
				</tr>
				<tr>
					<td><label>Email</label></td>
					<td><input type="text" name="mailId" id="mailId" /></td>
				</tr>
				<tr>
					<td><label>Password</label></td>
					<td><input type="text" name="password" id="password" /></td>
				</tr>
				<tr>
					<td><label>Designation</label></td>
					<td><input type="text" name="designation" id="designation" />
					</td>

				</tr>
				<tr>
					<td><label>Role</label></td>
					<td><select name="role">
							<option value="admin">Admin</option>
							<option value="employee">Employee</option>
							<option value="Manager">Manager</option>
					</select></td>
				</tr>
				<tr>
					<td><label>Projects</label></td>
					<td><select name="project" multiple="multiple">
							<option value="1">project1</option>
							<option value="2">project2</option>
							<option value="3">project3</option>
					</select></td>
				</tr>

			</table>
			<button type="submit" class="btn btn-default" id="addpro-submit">ADD
				EMPLOYEE</button>
		</form:form>
	</div>

</body>
</html>
