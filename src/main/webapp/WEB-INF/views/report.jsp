<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>REPORT</title>
</head>
<body>
	<div class="container"><%-- ${data.empDetails} --%>
		<h2>ADD EMP</h2>
		<br>
		<p style="color: red">${error_message}</p>
		<table border="1">
			<tr>
			<tr>
				<td>Department</td>
				<td>${data.empDetails.departmentId}</td>
			</tr>
			<tr>
				<td>Name</td>
				<td>${data.empDetails.name}</td>
			<tr>
			<tr>
				<td>Age</td>
				<td>${data.empDetails.age}</td>
			</tr>
			<tr>
				<td>Address</td>
				<td>${data.empDetails.address}</td>
			</tr>
			<tr>
				<td>email</td>
				<td>${data.empDetails.mailId}</td>
			</tr>
			<tr>
			<td>Designation</td>
			<td>${data.empDetails.designation}</td>
			</tr>
			<tr>
			<td>Projects</td>
			<td>${data.empDetails.projectList}</td>
			</tr>
</body>
</html>
