<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="log-row">
				<div class="col-md-4 col-md-offset-4 log-new">
					<form:form method="post" action="login" commandName="login" name="login" id="login">
						<div class="form-group">
							<label for="txt">Username</label> 
							<input type="text" class="form-control" name="userName" id="usererror" />
						</div>
						<div class="form-group">
							<label for="pwd"> Password </label> <input type="password" class="form-control" name="password" />
							<div class="butt-submit">
								<p style="color: red">${f_error_message}</p>
								<br />
								<button type="submit" class="btn btn-info">Login</button>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div>
	
</body>

</html>
